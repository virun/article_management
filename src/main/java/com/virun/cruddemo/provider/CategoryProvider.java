package com.virun.cruddemo.provider;

import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.util.Pagination;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String findAllCategories(Integer cat_id){
        return new SQL(){{
            SELECT("cat_id, cat_title");
            FROM("tb_category");
            if (cat_id!=null){
                WHERE("cat_id = "+cat_id);
            }
            ORDER_BY("cat_id");
        }}.toString();
    }

    public String getAllCategories(@Param("paging") Pagination paging){
        System.out.println(paging.getLimit()+ " | "+paging.getOffset());
        return new SQL(){{
            SELECT("cat_id, cat_title");
            FROM("tb_category");
            ORDER_BY("cat_id LIMIT #{paging.limit} OFFSET #{paging.offset}");
        }}.toString();
    }

    public String findNameCategory(String cat_title){
        return new SQL(){{
            SELECT("cat_id, cat_title");
            FROM("tb_category");
            WHERE("cat_title = "+ cat_title);
            ORDER_BY("cat_id");
        }}.toString();
    }

    public String remove(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("cat_id = "+id);
        }}.toString();
    }

    public String removeAll(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("cat_id = "+id);
        }}.toString();
    }

    public String add(@Param("category") Category category){
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("cat_title", "#{category.cat_title}");
        }}.toString();
    }

    public String update(@Param("cate") Category category){
        System.out.println("Update :" + category.toString());
        return new SQL(){{
            UPDATE("tb_category");
            SET("cat_title = #{cate.cat_title}");
            WHERE("cat_id = #{cate.cat_id}");
        }}.toString();
    }

    // Get count article in category
    public String getCountArticleCategory(@Param("cate") Category cate){
        System.out.println("Cate_ID " + cate.getCat_id());
        return new SQL(){{
            SELECT("COUNT(A.cat_id)");
            FROM("tb_article AS A");
            GROUP_BY("A.cat_id");
        }}.toString();
    }
    public String getCountCategory(){
        return new SQL(){{
            SELECT("COUNT(cat_id)");
            FROM("tb_category");
            GROUP_BY("cat_id");
        }}.toString();
    }
}
