package com.virun.cruddemo.model;

import com.github.javafaker.Cat;

import javax.validation.constraints.NotBlank;

public class Category {

    private Integer cat_id;
    @NotBlank
    private String cat_title;
    private Integer count_cat;

    public Category(){

    }

    public Category(Integer cat_id, String cat_title, Integer count_cat) {
        this.cat_id = cat_id;
        this.cat_title = cat_title;
        this.count_cat = count_cat;
    }

    public Integer getCount_cat() {
        return count_cat;
    }

    public void setCount_cat(Integer count_cat) {
        this.count_cat = count_cat;
    }

    public Integer getCat_id() {
        return cat_id;
    }

    public void setCat_id(Integer cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_title() {
        return cat_title;
    }

    public void setCat_title(String cat_title) {
        this.cat_title = cat_title;
    }

    @Override
    public String toString() {
        return "Category={" + "id=" + cat_id + ", title=" + cat_title + ", description=" + count_cat;
    }
}
