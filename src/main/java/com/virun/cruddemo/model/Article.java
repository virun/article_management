package com.virun.cruddemo.model;

import com.github.javafaker.Address;
import com.github.javafaker.FunnyName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {


    private Integer id;
    @NotBlank
    private String title;
    @NotBlank
    private String description;
    @NotBlank
    private String author;
    private String createdDate;
    private String image;

    private Integer cat_id;

    private String cat_title;
    public Article(){

    }

    public Article(Integer id,String title, String description, String author, String createdDate, String image, Integer cat_id, String cat_title) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
        this.image = image;
        this.cat_id = cat_id;
        this.cat_title = cat_title;
    }

    public long getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setCat_id(Integer cat_id) {
        this.cat_id = cat_id;
    }

    public Integer getCat_id() {
        return cat_id;
    }

    public void setCat_title(String cat_title) {
        this.cat_title = cat_title;
    }

    public String getCat_title() {
        return cat_title;
    }

    @Override
    public String toString() {
        return "Article{" + "id=" + id + ", title=" + title + ", description=" + description + " author= "+ author +"create_date= "+ createdDate + ", image=" + image + ", cat_id=" + cat_id + '}';
    }
}