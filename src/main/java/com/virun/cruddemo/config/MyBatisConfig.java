package com.virun.cruddemo.config;

import org.apache.ibatis.cache.TransactionalCacheManager;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hibernate.validator.constraints.URL;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.virun.cruddemo.repository")
public class MyBatisConfig {

//    @Autowired
//    DataSource dataSource;
//
//    @Bean
//    SqlSessionFactoryBean sqlSessionFactoryBean(){
//        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//        bean.setDataSource(dataSource);
//        return bean;
//    }
//
//    @Bean
//    DataSourceTransactionManager dataSourceTransactionManager(){
//        return new DataSourceTransactionManager(dataSource);
//    }

}
