package com.virun.cruddemo.util;

public class Pagination {

    private int totalPage, nextPage, prePage, totalCount, limit, page, showPage, stPage, edPage, offset;

    public Pagination(){
        this(0,0,7, 1, 3);
    }

    public Pagination(int totalPage, int totalCount, int limit, int page, int showPage) {
        this.totalPage = totalPage;
        this.totalCount = totalCount;
        this.limit = limit;
        this.page = page;
        this.showPage = showPage;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return (this.page-1)*this.limit;
    }

    public int getTotalPage() {
        return (int) Math.ceil((double)this.totalCount / limit);
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getNextPage() {
        return page >= getTotalPage()?getTotalPage():page+1;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public int getPrePage() {
        return page <= 1?1:page-1;
    }

    public void setPrePage(int prePage) {
        this.prePage = prePage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        this.setStartEndPage(getTotalPage());
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getShowPage() {
        return showPage;
    }

    public void setShowPage(int showPage) {
        this.showPage = showPage;
    }

    public int getStPage() {
        return stPage;
    }

    public void setStPage(int stPage) {
        this.stPage = stPage;
    }

    public int getEdPage() {
        return edPage;
    }

    public void setEdPage(int edPage) {
        this.edPage = edPage;
    }


    public void setStartEndPage(int totalPage){

        int pageToShow = this.showPage / 2;
        if (totalPage<=showPage){
            stPage = 1;
            edPage = totalPage;
        }
        else if (page - pageToShow <= 0){
            stPage = 1;
            edPage = showPage;
        }
        else if(page == totalPage){
            stPage = page - pageToShow-1;
            edPage = totalPage;
        }
        else if(page == totalPage-pageToShow){
            stPage = page - pageToShow;
            edPage = totalPage;
        }
        else{
            stPage = page - pageToShow;
            edPage = page + pageToShow;
        }

    }

    @Override
    public String toString() {
        return "Paging [page=" + page + ", limit=" + limit + ", totalCount=" + totalCount + ", totalPages=" + totalPage
                + ", nextPage=" + nextPage + ", previousPage=" + prePage + ", offset=" + offset + "]";
    }
}
