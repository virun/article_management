package com.virun.cruddemo.service;

import com.github.javafaker.Cat;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.repository.ArticleCategoryRepository;
import com.virun.cruddemo.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleCategoryImp implements ArticleCategoryService {

    private ArticleCategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(ArticleCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAllCategories() {

        return categoryRepository.findAllCategories();
    }

    @Override
    public List<Integer> getCountArticleCategory(Category category) {
        return categoryRepository.getCountArticleCategory(category);
    }

    @Override
    public Category findCatById(Integer cat_id) {
        return categoryRepository.findCatById(cat_id);
    }

    @Override
    public Category findCatByName(String cat_title) {
        return categoryRepository.findCatByName(cat_title);
    }

    @Override
    public void addArticleCategory(Category category) {
        categoryRepository.addArticleCategory(category);
    }

    @Override
    public void deleteArticleCategory(Integer id) {
        categoryRepository.deleteArticleCategory(id);
    }

    @Override
    public void deleteArticleCategoryAll(Integer id) {
        categoryRepository.deleteArticleCategoryAll(id);
    }

    @Override
    public void editArticleCategory(Category category) {
        categoryRepository.editArticleCategory(category);
    }

    @Override
    public List<Category> getAllCategories(Pagination pagination) {
        pagination.setTotalCount(categoryRepository.getCountCategory().size());
        return categoryRepository.getAllCategories(pagination);
    }
}
