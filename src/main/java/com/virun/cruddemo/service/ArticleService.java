package com.virun.cruddemo.service;

import com.virun.cruddemo.model.Article;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.util.Pagination;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService
{
    List<Article> findByName(String name);

    Article findById(Category category,Integer id);

    List<Article> findAll();

    void save(Article article);

    void delete(Integer id);

    void update(Article article);

    Integer getLastId();

    List<Article> getFilterPage(Category category, Pagination pagination);

    List<Article> getFilterCategory(String category);

    Article viewArticle(Integer id);

}
