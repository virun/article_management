package com.virun.cruddemo.service;

import com.virun.cruddemo.model.Article;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.repository.ArticleReposity;
import com.virun.cruddemo.util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService
{
    ArticleReposity articleReposity;
    Article article;
    List<Article> articles = new ArrayList<>();
    List<Integer> pages = new ArrayList<>();

    List<Article> articleList = new ArrayList<>();

    private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Autowired
    public void setUserRepository(ArticleReposity studentRepository) {
        this.articleReposity = studentRepository;
    }

    @Override
    public List<Article> findByName(String name) {
        return articleReposity.findByName(name);
    }

    @Override
    public Article findById(Category category, Integer id) {
        return articleReposity.findById(category,id);
    }

    @Override
    public List<Article> findAll() {

        return articleReposity.findAll();
    }

    @Override
    public void save(Article article) {
        article.setCreatedDate(sdf.format(new Date()));
        articleReposity.save(article);
    }

    @Override
    public void delete(Integer id) {
        articleReposity.delete(id);
    }

    @Override
    public void update(Article article) {
        article.setCreatedDate(sdf.format(new Date()));
        articleReposity.update(article);
    }

    @Override
    public Integer getLastId() {
        System.out.println("Last id: "+articleReposity.getLastId());
        return articleReposity.getLastId();
    }

    @Override
    public List<Article> getFilterPage(Category category, Pagination pagination) {
        pagination.setTotalCount(articleReposity.getCountAllFilter(category));
        return articleReposity.getFilterPage(category, pagination);
    }

    @Override
    public Article viewArticle(Integer id) {
        return articleReposity.viewArticle(id);
    }

    @Override
    public List<Article> getFilterCategory(String category) {
        return articleReposity.getFilterCategory(category);
    }

}
